package com.jerseydemo.repositories;

import com.jerseydemo.models.Student;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface StudentRepository {

    Student getStudent(int studentId);

    Student saveStudent(Student student);
}
