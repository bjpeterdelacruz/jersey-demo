package com.jerseydemo.repositories;

import com.jerseydemo.models.Student;
import org.jvnet.hk2.annotations.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentRepositoryStub implements StudentRepository {

    private final List<Student> students = new ArrayList<>();

    @Override
    public Student getStudent(int studentId) {
        if (studentId < 1 || studentId > students.size()) {
            return null;
        }
        return students.get(studentId - 1);
    }

    @Override
    public Student saveStudent(Student student) {
        var newStudent = new Student(students.size() + 1, student.firstName(), student.lastName(), student.major());
        students.add(newStudent);
        return newStudent;
    }
}
