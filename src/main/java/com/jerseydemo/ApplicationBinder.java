package com.jerseydemo;

import com.jerseydemo.repositories.StudentRepositoryStub;
import jakarta.ws.rs.ext.Provider;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

@Provider
public class ApplicationBinder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(new StudentRepositoryStub()).to(StudentRepositoryStub.class);
    }
}
