package com.jerseydemo;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

import java.util.Map;

@ApplicationPath("/api")
public class ApplicationConfig extends Application {
    @Override
    public Map<String, Object> getProperties() {
        return Map.of("jersey.config.server.provider.packages", "com.jerseydemo.services");
    }
}
