package com.jerseydemo.services;

import com.jerseydemo.models.Student;
import com.jerseydemo.repositories.StudentRepository;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/student")
public class StudentService {

    @Inject
    private StudentRepository studentRepository;

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStudent(@PathParam("id") int id) {
        var student = studentRepository.getStudent(id);
        if (student == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.status(Response.Status.OK).entity(student).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveStudent(Student student) {
        return Response.status(Response.Status.CREATED)
                .entity(studentRepository.saveStudent(student)).build();
    }
}
