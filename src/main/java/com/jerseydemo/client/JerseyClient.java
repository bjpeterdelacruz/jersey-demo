package com.jerseydemo.client;

import com.jerseydemo.models.Student;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;

public class JerseyClient {
    private static final String REST_URI = "http://localhost:8080/jerseydemo/api/student";

    private static final Client CLIENT = ClientBuilder.newClient();

    public static void main(String... args) {
        var student = CLIENT.target(REST_URI)
                .path(String.valueOf(1)).request(MediaType.APPLICATION_JSON).get(Student.class);
        System.out.println(student);

        var newStudent = new Student(-1, "Bill", "Paxton", "History");
        try (var response = CLIENT.target(REST_URI)
                .request(MediaType.APPLICATION_JSON).post(Entity.entity(newStudent, MediaType.APPLICATION_JSON))) {
            System.out.println(response.readEntity(Student.class));
        }
    }
}
