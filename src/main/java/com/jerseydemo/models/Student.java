package com.jerseydemo.models;

public record Student(int id, String firstName, String lastName, String major) {
}
